tariffs = {
    0: 0.3655,
    1: 0.4080,
    2: 0.4080,
    3: 0.52
}

scales = {
    0: 0,
    1: 19982,
    2: 33791,
    3: 67072,
    }

discs = {
    0:"Schijf 1",
    1:"Schijf 2",
    2:"Schijf 3",
    3:"Schijf 4"
}


print("Uw loon in euro's?")
i = int(input())
for s in range(4):
    #if remainder < 0 just print 0
    if i <= 0:
        print(discs[s], ": 0")
    #if remainder is within scale, multiply and set remainder to 0
    elif i < (int(scales[s+1] - int(scales[s]))):
        print(discs[s], ": ", float(tariffs[s]), " over ", i, " = ", int(i * tariffs[s]))
        i = 0
    else:
    #if remainder larger than scale, multiply scale
        y = scales[s+1] - scales[s]
        print(discs[s], ": ", float(tariffs[s]), " over ", y, " = ", int(y * tariffs[s]))
        i = i - y

    