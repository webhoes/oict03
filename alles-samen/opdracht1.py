
def print_menu():
    print("******************")
    print("*    Drawings!   *")
    print("******************")
    print("* 1) Draw a house  ")
    print("* 2) Draw a pyramid")
    print("* 0) Exit           ")
    print("******************")
    x = int(input())
   
    if x == 1:
        draw_house()
    elif x == 2:
        draw_piramide()
    elif x == 0:
        exit()
    else: 
        print("No valid input. Choose again.")
        print_menu()

def draw_house():
    print("   +   ")
    print("  + +  ")
    print(" +   + ")
    print("+-----+")
    print("| | | |")
    print("| | | |")
    print("+_+_+_|")
    print_menu()

def draw_piramide():
    print("   +   ")
    print("  + +  ")
    print(" +   + ")
    print("+++++++")
    print_menu()


print_menu()