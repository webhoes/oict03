<?php
// if no value for $r set new random in otherwise take from form
if(isset($_POST['random'])){ $r = $_POST['random'];} else {$r = random_int(0,100);}
// if no value from form set $n to -1 otherwise take from form
if(isset($_POST['number'])) {$n = $_POST['number'];} else {$n = -1;}


if ($n < $r && $n>0){ // check if $n is smaller than $r and bigger than 0
    echo "<h3>Too low</h3><br>";
    echo '
<h3>Choose a new number</h3>
<form method="post" action="">
<input type="number" name="number">
<input type="hidden" name="random" value="' . $r .'">
<input type="submit">
</form>';

} elseif($n > $r) { // check if $n is too high
    echo "<h3>Too high</h3><br>";
    echo '
<h3>Choose a new number</h3>
<form method="post" action="">
<input type="number" name="number">
<input type="hidden" name="random" value="' . $r .'">
<input type="submit">
</form>';
} elseif ($n==$r){ // correct answer
    echo "<h3>You guessed it</h3>";
} else { // initial state of form
echo '
<h3>Your number</h3>
<form method="post" action="">
<input type="number" name="number">
<input type="hidden" name="random" value="' . $r .'">
<input type="submit">
</form>';
}