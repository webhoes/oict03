<?php
if (isset($_POST['number'])) {
    $size = $_POST['number'];
    $c = false; //if input is not correct, second block will not run
    switch ($size) {
        case 1:
            $fields = 9;
            $c = true;
            break;
        case 2:
            $fields = 13;
            $c = true;
            break;
        case 3:
            $fields = 19;
            $c = true;
            break;
        default:
            echo "Choose between 1, 2 or 3";
    }
    if ($c) {
        $rows = 0;
        while ($rows < $fields) {
            $columns = 0;
            while ($columns < $fields) {
                echo "[]";
                $columns++;
            }
            echo "<br>";
            $rows++;
        }
    }
} else {
    //render form
    echo '
    1 - Small (9 x 9)<br>
    2 - Medium (13 x 13)<br>
    3 - Large (19 x 19)<br>
    <h3>Size of the board</h3>
    <form method="post" action="">
    <input type="number" name="number">
    <input type="submit" name="submit" value ="Choose">
</form>';
}
