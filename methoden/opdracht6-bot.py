#tic tac toe

import random

# one array from 1 - 9 but printed over 3 lines
# replace number with x or o
# if on replace not a number generate error
# if number 1 < or number > 9 generate error

board = [1,2,3,4,5,6,7,8,9]
checks = [[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]] # all possible winning arrays
array_values = [0,0,0,0,0,0,0,0] # value of each individual winning array
possible_moves = [1,3,7,9,5,2,4,6,8] # in order of preference 
max_move_counter = 0

# player 1 is +1
# player 2 is - 1
# free is 0 


user = random.randint(1,2)
if user == 1:
    comp = 2
else:
    comp = 1

player = 1

print("you are player ", user)
print("Computer is player ", comp)
print("Player 1 get ready to start!")

def print_board():
    v=0
    for n in board:
        print (n, end = ' ')
        v = v + 1
        if v % 3 == 0:
            print("\n")
    
def calculate_array_value():
    for check in range(len(checks)):# multi-dimensional array with all possible win options
        array_value_counter = 0 # set counter to 0 for next sub array
        for value in range(len(checks[check])): # get indexes for sub array
            print("value in check ", checks[[value][check]])
            if board[[value][check]] == "X":array_value_counter = array_value_counter +1
            elif board[[value][check]] == "Y": array_value_counter = array_value_counter -1
            else: array_value_counter = array_value_counter
    array_values[check] = array_value_counter
    print (array_value_counter)

def print_array_value():
    for v in array_values:
        print(array_values[v])                        

def check_winner():
   for winner in range(len(checks)):
        if checks[winner] == 3: print("X is winner")
        else: continue
        if checks[winner] == -3: print("Y is winner")
        else: continue
        
def comp_move():
    # check to make winning move
    # check to block winning move user
    # check for free corner places
    # check for free center center place
    
        
    possible_moves = [1,3,7,9,5,2,4,6,8] #in order of preference
    search = True
    
        
    for i in board:
        while search == True:
            search = False
            for y in possible_moves:
                if i == y:
                    if player == 1: output = "X"
                    if player == 2: output = "O"              
                    board[i-1] = output
                else:
                    search = True
                    

print_board()#initial print of the board

while(max_move_counter < 9):
    #print_board()
   
   
    print("Player: ", player)

    if player == user:
    
           
        x = int(input())
        if x < 1: print("too low")
        if x > 9: print("too high")
        if board[x-1] == "X": print("Already taken, choose again")
        if board[x-1] == "O": print("Already taken, choose again")

        if player == 1: output = "X"
        if player == 2: output = "O"
        print("user")
        board[x-1] = output
            
    else:
        #comp_move()
        #horizontal
        

        #check for winning move
        # for check in range(len(checks)):
        #     count = 0
        #     for c in range(len(checks[check])):
        #         if c == sign:
        #             count = count + 1

        #posible moves
        found = 0
          
        for i in possible_moves:
            if found == 1: break
            try:
                int(i)
            except:
                continue
            else:
               # if board[int(i)-1] == "X" or board[int(i)-1] == "O": #position is taken
               #     continue
               # else: 
                for s in board:
                    if s == i:
                        if player == 1: output = "X"
                        if player == 2: output = "O"
                        print("comp")
                        board[i-1] = output
                        found = 1
                        break
                    else: #position is free
                        continue            
            
    print_board() #print updated board for next round

    if player == 1: 
        player = 2
    else:
        player = 1                            
    max_move_counter = max_move_counter + 1
            
    check_winner()
    calculate_array_value()
    print_array_value()

print("no winner")


