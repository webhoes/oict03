#tic tac toe

# one array from 1 - 9 but printed over 3 lines
# replace number with x or o
# if on replace not a number generate error
# if number 1 < or number > 9 generate error

board = [1,2,3,4,5,6,7,8,9]
c= 0
player = 1

print("Player 1 is X")
print("Player 2 is O")
print("Player 1 get ready to start!")

def print_board():
    v=0
    for n in board:
        print (n, end = ' ')
        v = v + 1
        if v % 3 == 0:
            print("\n")

def check_winner(player):
    # horizontal check 3 consequtive number starting from 1, 4, 7   
    # vertical check 1 + 3 + 3; 2 + 3 + 3, 3 + 3 + 3
    # diagonal check 1 + 4 + 4; 3 + 2 + 2;
    # horizontal
    # 123
    # 456
    # 789
    # vertical
    # 147
    # 258
    # 369
    # diagonal
    # 159
    # 357

    if board[0] == board[1] == board[2]:
        print('Player ', player, ' is the winner')

    if board[3] == board[4] == board[5]:
        print('Player ', player, ' is the winner')

    if board[6] == board[7] == board[8]:
        print('Player ', player, ' is the winner')

    if board[0] == board[3] == board[6]:
        print('Player ', player, ' is the winner')

    if board[0] == board[4] == board[8]:
        print('Player ', player, ' is the winner')

    if board[1] == board[4] == board[7]:
        print('Player ', player, ' is the winner')

    if board[2] == board[5] == board[8]:
        print('Player ', player, ' is the winner')                
        

def check_max_moves():
    if c == 9:
        print("No winner")
        

while(c < 9):
    print_board()
   
   # check input
    go = True
    while go == True:
        go = False
        x = int(input())
        if x < 1: print("too low"); go = True
        if x > 9: print("too high"); go = True
        if board[x-1] == "X": print("Already taken, choose again"); go = True
        if board[x-1] == "O": print("Already taken, choose again"); go = True

    if player == 1: output = "X"
    if player == 2: output = "O"
    board[x-1] = output
    c = c + 1
    
    check_winner(player)

    check_max_moves()

    if player == 1: 
        player = 2
    else:
        player = 1