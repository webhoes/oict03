<?php
if (isset($_POST['price']) && isset($_POST['paid'])) {
    (int) $price = $_POST['price'] * 100;
    (int) $paid = $_POST['paid'] * 100;

    if ($paid < $price) {
        echo "Not paid enough";
    } else {
        echo "Price is " . $price / 100 . "<br>";
        echo "Paid " . $paid / 100 . "<br>";
        $change = $paid - $price;
        echo "Change is " . $change / 100 . "<br>";

        $steps = array(10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5);
        foreach ($steps as $step) {
            if ($change >= $step) {
                $count = (floor($change / $step));
                echo "Euro " . $step / 100 . ": " . $count . " - Change left is " . (($change / 100) - ($count * $step / 100)) . "<br>";
                $change = $change % $step;
            } else {
                echo "Euro " . $step / 100 . ": 0 - Change left is " . $change / 100 . "<br>";
            }
        }
    }
} else {
    echo '
    <h3>Type 2 numbers</h3>
    <form method="post" action="">
    <input type="number" name="price">
    <input type="number" name="paid">
    <input type="submit">
</form>';
}
