<?php
if (isset($_POST['number1']) && isset($_POST['number2'])) {
    (int)$number1 = $_POST['number1'];
    (int)$number2 = $_POST['number2'];

    echo $number1 . " + " . $number2 . " = " . ($number1 + $number2) . "<br>";
    echo $number1 . " - " . $number2 . " = " . ($number1 - $number2) . "<br>";
    echo $number2 . " - " . $number1 . " = " . ($number2 - $number1) . "<br>";
    echo $number1 . " X " . $number2 . " = " . ($number1 * $number2) . "<br>";
    echo $number1 . " / " . $number2 . " = " . ($number1 / $number2) . "<br>";
    echo $number2 . " / " . $number1 . " = " . ($number2 / $number1) . "<br>";
    echo $number1 . " % " . $number2 . " = " . ($number1 % $number2) . "<br>";
    echo $number2 . " % " . $number1 . " = " . ($number2 % $number1);

} else {
    echo '</h3>
    <h3>Type 2 numbers
    <form method="post" action="">
    <input type="number" name="number1">
    <input type="number" name="number2">

    <input type="submit">
</form>';
}
?>